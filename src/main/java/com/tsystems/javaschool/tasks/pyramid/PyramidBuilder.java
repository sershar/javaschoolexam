package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        int listSize, strNum, colNum, iNumber, iStr, iCol, startCol;
        double calcStrNum;
        int[][] pyramid;

        listSize = inputNumbers.size();
        calcStrNum = (-1 + Math.sqrt(1 + 8 * listSize)) / 2;

        if (Math.ceil(calcStrNum) == calcStrNum) {
            strNum = (int)Math.ceil(calcStrNum);
        }
        else
            throw new CannotBuildPyramidException();

        colNum = 2 * strNum - 1;

        pyramid = new int[strNum][colNum];

        try {
            Collections.sort(inputNumbers);
        }catch(Exception e)
        {
            throw new CannotBuildPyramidException();
        }

        startCol = colNum - 1;
        iNumber = listSize - 1;

        for(iStr = strNum - 1; iStr >= 0; iStr--)
        {
            iCol = startCol;
            for(int i = 0; i <= iStr; i++)
            {
                pyramid[iStr][iCol] = inputNumbers.get(iNumber);
                iNumber--;
                iCol -= 2;
            }
            startCol--;
        }

        return pyramid;
    }


}
