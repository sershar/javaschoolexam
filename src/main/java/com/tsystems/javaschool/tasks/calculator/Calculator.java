package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    private class listObject{
        double number;
        boolean isSign;
        char c;

        public listObject(double number) {
            this.number = number;
            isSign = false;
        }

        public listObject(char c) {
            this.c = c;
            isSign = true;
        }
    }

    private int priority(char c1, char c2)
    {
        if(c1 == '+' || c1 == '-')
        {
            if(c2 == '+' || c2 == '-')
                return 0;
            else
                return -1;
        }else
        {
            if(c2 == '+' || c2 == '-')
                return 1;
            else
                return 0;
        }
    }

    private boolean isDigit(char c) {
        return (c >= '0') && (c <= '9');
    }

    private boolean isMathSign(char c) { return c == '+' || c == '-' || c == '*' || c == '/'; }

    private double convertToNumber(String statement, int start, int end) throws Exception
    {
        boolean foundPoint = false;
        int iPoint = 0;
        double sum = 0;

        if (statement.charAt(start) == '.' || statement.charAt(end) == '.')
            throw new Exception();

        for(int i = start; i <= end; i++) {
            if (statement.charAt(i) == '.') {
                if (foundPoint) {
                    throw new Exception();
                }
                foundPoint = true;
                iPoint = i;
                //break;
            }
        }

        if (!foundPoint) {
            iPoint = end + 1;
        }

        boolean decimalPart = false;

        for (int i = start; i <= end; i++)
        {
            if(statement.charAt(i) == '.') {
                decimalPart = true;
                continue;
            }
            else
            {
                sum = sum + (statement.charAt(i) - '0') * Math.pow(10, iPoint - i + (decimalPart?0:-1));
            }
        }
        return sum;
    }

    private boolean checkParentheses(String statement){
        int numberOpenedParentheses = 0;
        boolean first = true;

        for(int i = 0; i < statement.length(); i++){
            if(statement.charAt(i) == '(') {
                numberOpenedParentheses++;
                first = false;
            }
            else if(statement.charAt(i) == ')') {
                if (first)
                    return false;
                numberOpenedParentheses--;
            }
        }

        return (numberOpenedParentheses == 0);
    }

    private boolean checkOrder(String statement)
    {
        boolean wasNumber = false, wasSign = false;

        for(int i = 0; i < statement.length(); i++)
        {
            if(isDigit(statement.charAt(i)) || statement.charAt(i) == '.')
            {
                if (wasNumber)
                    return false;
                wasNumber = true;
                wasSign = false;

                while (i < statement.length() && (isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) i++;
                i--;
            }
            else {
                if (isMathSign(statement.charAt(i))) {
                    if (wasSign)
                        return false;
                    wasNumber = false;
                    wasSign = true;
                }else
                {
                    if (statement.charAt(i) == '(' || statement.charAt(i) == ')')
                    {
                        if (statement.charAt(i) == '(') {
                            if(wasNumber)
                                return false;
                            //wasNumber = false;
                            wasSign = false;
                        }
                        else {
                            if(wasSign)
                                return false;
                            wasNumber = true;
                        }
                    }
                    else
                        return false;
                }
            }
        }
        return true;
    }

    private boolean generateList(String statement, ArrayList<listObject> list)
    {
        int iValStart = 0, iValEnd;
        boolean nowNumber = false;
        double number;
        ArrayList<Character> stack = new ArrayList<Character>();

        if(statement == null || statement.equals("") || !checkParentheses(statement) || !checkOrder(statement))
            return false;

        for (int i = 0; i < statement.length(); i++) {
            if (isDigit(statement.charAt(i)) || statement.charAt(i) == '.') {
                if (!nowNumber) {
                    iValStart = i;
                    nowNumber = true;
                }

                if(i == statement.length() - 1 || (!isDigit(statement.charAt(i + 1)) && statement.charAt(i + 1) != '.'))
                {
                    iValEnd = i;
                    nowNumber = false;
                    try {
                        number = convertToNumber(statement, iValStart, iValEnd);
                    }catch (Exception e) {
                        return false;
                    }
                    list.add(new listObject(number));
                }
            } else {
                switch (statement.charAt(i))
                {
                    case '(':
                        stack.add('(');
                        break;
                    case ')':
                        while(stack.get(stack.size() - 1) != '(')
                        {
                            list.add(new listObject(stack.get(stack.size() - 1)));
                            stack.remove(stack.size() - 1);
                        }
                        stack.remove(stack.size() - 1);
                        break;
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                        while(stack.size() > 0 && stack.get(stack.size() - 1) != '(' && (priority(stack.get(stack.size() - 1), statement.charAt(i)) >= 0))
                        {
                            list.add(new listObject(stack.get(stack.size() - 1)));
                            stack.remove(stack.size() - 1);
                        }
                        stack.add(statement.charAt(i));
                        break;
                    default:
                        break;
                }
            }
        }

        while(stack.size() > 0)
        {
            list.add(new listObject(stack.get(stack.size() - 1)));
            stack.remove(stack.size() - 1);
        }
        return true;
    }

    private double count(ArrayList<listObject> list) throws Exception
    {
        int iSign = 0;

        while(list.size() > 2)
        {
            for(int i = 0; i < list.size(); i++)
            {
                if(list.get(i).isSign) {
                    iSign = i;
                    break;
                }
            }

            switch(list.get(iSign).c)
            {
                case '+':
                    list.get(iSign).number = list.get(iSign - 2).number + list.get(iSign - 1).number;
                    break;
                case '-':
                    list.get(iSign).number = list.get(iSign - 2).number - list.get(iSign - 1).number;
                    break;
                case '*':
                    list.get(iSign).number = list.get(iSign - 2).number * list.get(iSign - 1).number;
                    break;
                case '/':
                    if (list.get(iSign - 1).number != 0)
                        list.get(iSign).number = list.get(iSign - 2).number / list.get(iSign - 1).number;
                    else
                        throw new Exception();
                    break;
                default:
                    throw new Exception();
            }
            list.get(iSign).isSign = false;
            list.remove(iSign - 1);
            list.remove(iSign - 2);
        }
        return list.get(0).number;
    }

    private String removeZerosAndComa(String str)
    {
        boolean foundSignifDigit = false;
        int iComa = str.length() - 5, iLastSignifDigit = str.length() - 1;

        for(int i = str.length() - 1; i > 0; i--) {
            if(!foundSignifDigit && str.charAt(i) != '0') {
                iLastSignifDigit = i;
                foundSignifDigit = true;
            }
        }

        if(iLastSignifDigit == iComa)
            return str.substring(0, iComa);
        else
            return str.substring(0, iComa).concat(".".concat(str.substring(iComa + 1, iLastSignifDigit + 1)));
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        ArrayList<listObject> list = new ArrayList<listObject>();
        double number;

        if(generateList(statement, list))
        {
            try {
                number = count(list);
            }catch(Exception e) {
                return null;
            }
            return removeZerosAndComa(String.format("%.4f", number));
        }

        return null;
    }
}
